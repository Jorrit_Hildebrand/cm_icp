#include "icp.h"
#include "uart.h"

static int closestNeighbour(point_t a);
static double calcDistance(point_t a, point_t b);

static void loadMap(char *txt, char cutter);
static void getSensorData();
static void rotateSensorData(double angle);
static void moveSensorData(double x, double y);
static int cmp_ret(const void *aptr, const void *bptr);

static point_t sensor[SENSORCOUNT] = {0};
static int fd = 0;

void icp_init()
{
  loadMap("./data/dataB.csv", ',');
  fd = initUart("/dev/ttyUSB0");
}

static double calcDistance(point_t a, point_t b)
{
  //return the distance squared between both points
  return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}


static int closestNeighbour(point_t a)
{
  double distance = 0, lowest = 100;
  int position = 0;

  /*
      TODO
      de map is geschaald samen met x, hoe hoger je in het array komt van map hoe hoger je komt met x. Deze feature kan gebruikt
      worden om niet altijd alle punten af te gaan
  */

  for (int i = 0; i < MAXPOINTS; i++) {
    distance  = calcDistance(a, map[i]);
    if (distance < lowest) {
      lowest = distance;
      position = i;
    }
  }
  debug("Smalest distance = %f, position = %d\n", lowest, position);
  return position;
}

void icp_offsetCoordinates(double *x, double *y, double *theta)
{
  int bestposition[5] = {0};
  double disX = 0, disY = 0;
  double angle = 0;
  double angles[5] = {0};

  debug("OFFSETCOORDINATES %f, %f, Angle: %f\n", *x, *y, *theta);
  getSensorData();
  moveSensorData(*x, *y);
  rotateSensorData(*theta);


  //loop for smallest error=
  for (int aaa = 0; aaa < SAMPLES; aaa++) {

    for (int i = 0; i < SENSORCOUNT; i++) {
      bestposition[i] = closestNeighbour(sensor[i]);
    }

    for (int i = 0; i < SENSORCOUNT; i++) {
      disX += map[bestposition[i]].x - sensor[i].x;
      disY += map[bestposition[i]].y - sensor[i].y;
    }

    disX = disX / SENSORCOUNT;
    disY = disY / SENSORCOUNT;
    moveSensorData(disX, disY);

    for (int i = 0; i < SENSORCOUNT; i++) {
      bestposition[i] = closestNeighbour(sensor[i]);
    }

    for (int i = 0; i < SENSORCOUNT; i++) {
      debug("(%f, %f) \t (%f, %f) \n", sensor[i].y, sensor[i].x, map[bestposition[i]].y, map[bestposition[i]].x);
      angles[i] =  atan(sensor[i].y/sensor[i].x) - atan(map[bestposition[i]].y/map[bestposition[i]].x);
      debug("angle %f, a: %f, b: %f \n", angles[i], tempia, tempib);
      angle += angles[i];
    }

    rotateSensorData(angle/10);

    //transform base place
    *theta = angle/5 + *theta;
    *x += disX;
    *y += disY;
    for (int i = 0; i < 5; i++) {
      angles[i] = 0;
    }
  }
  /*
  printf("#x,y\n");
  for (int i = 0; i < SENSORCOUNT; i++) printf("%f, %f\n", sensor[i].x, sensor[i].y);
  */
  
}

//theta in radian
static void rotateSensorData(double theta)
{
  double c = cos(theta);
  double s = sin(theta);
  point_t temp;

  for (int i = 0; i < SENSORCOUNT; i++) {
    debug("x : %f, y = %f",sensor[i].x, sensor[i].y);
    temp.x = sensor[i].x*c-sensor[i].y*s;
    temp.y = sensor[i].x*s+sensor[i].y*c;

    sensor[i].x = temp.x;
    sensor[i].y = temp.y;
    debug("\tx : %f, y = %f\n",sensor[i].x, sensor[i].y);
  }
}

static void moveSensorData(double x, double y)
{

  debug("x:%f, y:%f\n", x, y);

  for (int i = 0; i < SENSORCOUNT; i++) {
    sensor[i].x += x;
    sensor[i].y += y;
    }
}

static void loadMap(char *txt, char cutter)
{
  char c;

  FILE *fp;
  char temp[24] = {0};
  int tempi = 0, placenumber = 0, xory = 0;

  fp = fopen(txt, "r");

  if (fp == NULL){
      printf("Could not open file");
  }
  else{
    while (!feof(fp)){
      for (int i = 0; i < 24; i++) temp[i] = 0;
      c = fgetc(fp);
      if(c == '#'){
        while (c != '\n' && c != EOF) {
          c = fgetc(fp);
      }
    }
      else{
        while(c != cutter && c != '\n' && c != EOF){
          temp[tempi] = c;
          tempi++;
          c = fgetc(fp);
        }
        xory++;
        tempi = 0;
        if (xory % 2){
          map[placenumber].x = atof(temp);
          //debug("x: %4f\t", map[placenumber].x);
        }else{
          map[placenumber].y = atof(temp);
          //debug("y: %4f \n", map[placenumber].y;
          placenumber++;
        }
      }
    }
    fclose(fp);
  }

  qsort(map, MAXPOINTS, sizeof(point_t), cmp_ret);
}

static int cmp_ret(const void *aptr, const void *bptr)
{
    int a = ((point_t *)aptr)->x, b = ((point_t *)bptr)->x;
    return (a > b) - (a < b);
}

static void getSensorData()
{
  //testing for now, will get the data from a stdout or from a uart
  sensor[0].x = 2.458;
  sensor[0].y = 5.215;
  sensor[1].x = 4;
  sensor[1].y = 1.84523;
  sensor[2].x = 3.8568;
  sensor[2].y = 0;
  sensor[3].x = 3.8512;
  sensor[3].y = -1.7655;
  sensor[4].x = 2.432;
  sensor[4].y = -4.7589;

/*
  unsigned char buf[124];
  uint8_t rdlen;
  rdlen = read(fd, buf, sizeof(buf) - 1);
  //data format == a.x,a.y,b.x,b.y etc seperator = , first x then y
  for (uint8_t i = 0; i < SENSORCOUNT; i++) {
    sensor[i].x =
    sensor[i].y =
  }



  */
}
