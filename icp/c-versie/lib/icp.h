#ifndef ICP_H
#define ICP_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>

#define DEBUG 0
#if DEBUG
#define debug(fmt, ...) fprintf(stderr,"[DEBUG]: " fmt, ##__VA_ARGS__)
#else
#define debug(fmt, ...)
#endif

#define SAMPLES 5

#define DEG2RAD(d) ((d)*(M_PI/180.0))
#define RAD2DEG(d) ((d)*(M_PI/180))

#define SENSORCOUNT 5
#define MAXPOINTS 1544

typedef struct point_t{
  double x,y;
}point_t;

point_t map[MAXPOINTS];

void icp_offsetCoordinates(double *x, double *y, double *angle);
void icp_init(void);

#endif
