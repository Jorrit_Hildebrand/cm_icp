from PIL import Image
import numpy as np
from pyproj import Proj, transform

im = Image.open('./lijntjes/map2.bmp', 'r')
ncol, nrow = im.size
pixel_values = list(im.getdata())

coordinates = np.empty((1, 2))
temp = []
np.set_printoptions(suppress=True)

for waarde in pixel_values:
    if(str(waarde) in "(255, 255, 255, 255) "):
        temp.append(0)
    else:
        temp.append(1)

inProj = Proj(init='epsg:3857')
outProj = Proj(init='epsg:4326')
scale = 0.0205128205
for i in range(nrow):
    for j in range(ncol):
        if temp[ncol*i+j] is 1:
            x1, y1 = i*scale, j*scale
            x2, y2 = transform(inProj, outProj, x1, y1)
            print (x2, y2)
