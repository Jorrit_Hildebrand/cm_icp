#ifndef _UART_H_
#define _UART_H_

int set_interface_attribs(int fd, int speed);
int writeBytes(int fd, char* writingdata);
void set_mincount(int fd, int mcount);
unsigned char readonebyte(int fd);
int initUart(char *portname);
int readLine(char *line, int len, int fd);

#endif /* _UART_H_ */
