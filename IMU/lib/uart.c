#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>

#define LINE_LEN 1024
#define TIMEOUT 100

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    // ignore modem controls
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         // 8-bit characters
    tty.c_cflag &= ~PARENB;     // no parity bit
    tty.c_cflag &= ~CSTOPB;     // only need 1 stop bit
    tty.c_cflag &= ~CRTSCTS;    // no hardware flowcontrol

    // setup for non-canonical mode
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    // fetch bytes as they become available
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

unsigned char readonebyte(int fd)
{
  char b = 0;
  int timeout = 0;
  while((read(fd, &b, 1) != 1) && (timeout++ < TIMEOUT)){
  }
  return (b != 0) ? (b) : (0);;
}

/* Returns 0 when there is no line found
 * If there is a line found return the length of the line
 */
int readLine(char *line, int len, int fd)
{
  int placement = 0;
  for (int i = 0; i < len; i++) {
    line[i] = 0;
  }
  for (int i = 0; i < len; i++) {
    line[placement++] = readonebyte(fd);
    if (line[placement-1] == '\n')
      return i;
  }
  return 0;
}

int writeBytes(int fd, char* writingdata)
{
  int wlen;
  wlen = write(fd, writingdata, sizeof(writingdata)+1);
  if (wlen != 7)
  {
    tcdrain(fd);    // delay for output
    return -1;
  }
  else
  {
    tcdrain(fd);    // delay for output
    return 1;
  }
  return 0;
}

// 0 if succes, -1 when there was a error opening the port
int initUart(char *portname)
{

  int fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
  if (fd < 0) {
    printf("Error opening %s: %s\n", portname, strerror(errno));
    return -1;
  }
  //baudrate 115200, 8 bits, no parity, 1 stop bit
  set_interface_attribs(fd, B115200);
  return fd;
}
