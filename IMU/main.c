#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include "lib/uart.h"

#define LINE_LEN 1024

static double getHeading(char line[LINE_LEN]);

int main (int argc, char **argv){
  char line[LINE_LEN];
  double heading = 0.0;

  int fd = initUart("/dev/ttyACM0");
  if (fd < 0)
    exit(1);

  while (readLine(line, LINE_LEN, fd) != 0) {
    heading = getHeading(line);
    printf("%f\n", heading);
  }
  close(fd);
  return(0);
}

static double getHeading(char line[LINE_LEN]){
  double heading;
  char *here;
  if (line == NULL) {
    fprintf(stderr, "*** No valid line in grepHeading\n");
    return 0;
  }
  here = line;
  for (int i = 0; i < 3; i++) {
    while (*(here++) != ',');
  }
  heading = strtod(here, 0);

  return heading;
} /* getHeading */
