/*------------------------------------------------------------------------------
run with sudo ./start.sh
Beginning for the rpi code
-------------------------------------------------------------------------------
Plan:
Sensor A, B, D and E can scan for a wall. When sensor C returns something there
is a obstacle in front of the car or there is a error. With a predication model
we can plan our route so the EVA knows where to go.
The sensor data can be read out of the struct
------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <unistd.h>
#include "lib/uart.h"

#include <time.h>

#define SIZE 90
#define MAXNUM 1200
#define SCALESHOW (MAXNUM/SIZE)

//can be optimalized alot
#define DEG2RAD(d) ((d)*(M_PI/180.0))
#define ANGLEFTLEFT DEG2RAD(160.0)
#define ANGLEFT DEG2RAD(130.0)
#define ANGMIDDLE  DEG2RAD(90.0)
#define ANGRIGHT DEG2RAD(50.0)
#define ANGRIGHTRIGHT DEG2RAD(20.0)

typedef struct point_t{
  double x;
  double y;
}point_t;

void ShowGraph(point_t A, point_t B, point_t C, point_t D, point_t E);
int *readData(int fd, int *values);
void calcPoint(int lena, double direction, point_t *point);
void calcPointLookup(int lena, int angle, point_t *point);
double distance2wall(point_t *A, point_t *B);

static const int8_t lookupsin[10] =
{
  28, -119, 114, -34, 117,
  125, 77, 80, 124, 78
};


int main()
{
  point_t A, B, C, D, E;
  double left = 0, right = 0;
  int sensdata[2] = {0};
  int fd = initUart("/dev/ttyACM0");

  while(1){
    printf("Henlo\n");
    int a = readData(fd, sensdata);
    printf("%d\n", a);
  //  left = distance2wall(&A, &B);
    //right = distance2wall(&D, &E);
    //ShowGraph(A, B, C, D, E);
    //printf("%4.2f,%4.2f %4.2f,%4.2f %4.2f,%4.2f %4.2f,%4.2f %4.2f,%4.2f left: %4.2f, right: %4.2f\n\n\n\n", A.x, A.y, B.x,B.y, C.x,C.y, D.x,D.y, E.x, E.y, left, right);
    sleep(1);
    //fseek(stdin,0,SEEK_END);
  }
}

int *readData(int fd, int *values)
{
  unsigned char buf[10] = {0};

  while(1) {
    buf[1] = buf[0] = 0;
  do {
    printf("%c \n", buf[0]);
    buf[1] = buf[0];
    buf[0] = readonebyte(fd);
  } while(!(buf[0] == 0x59 && buf[1] == 0x59));

    for(int i = 2; i < 9; i++)
      buf[i] = readonebyte(fd);

    //values[0] = distance, values[1] = signal strength
    values[0] = (buf[2] | (buf[3] << 8));
    values[1] = (buf[4] | (buf[5] << 8));
    unsigned char checksum = buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7];
    //printf("%.2x %.2x %.4u %.4u %.2x %.2x\n", buf[0], buf[1], values[0], values[1], buf[8], checksum);
    if (checksum != buf[8])
      return NULL;
    else
      return values;
  }
}

void ShowGraph(point_t A, point_t B, point_t C, point_t D, point_t E)
{
  int drawfield[SIZE+1][SIZE+1] = {0};

  for (int y = 0; y < SIZE+1; y++) {
    for (int x = 0; x < SIZE+1; x++) {
        drawfield[y][x] = ' ';
      }
    }

//draw the distance on the drawfield
drawfield[(int)A.y/SCALESHOW][(int)(A.x+MAXNUM/2)/SCALESHOW] = '1';
drawfield[(int)B.y/SCALESHOW][(int)(B.x+MAXNUM/2)/SCALESHOW] = '2';
drawfield[(int)C.y/SCALESHOW][(int)(C.x+MAXNUM/2)/SCALESHOW] = '3';
drawfield[(int)D.y/SCALESHOW][(int)(D.x+MAXNUM/2)/SCALESHOW] = '4';
drawfield[(int)E.y/SCALESHOW][(int)(E.x+MAXNUM/2)/SCALESHOW] = '5';


  //flip the y-axis
    for (int y = SIZE-1; y >= 0; y--){
      for (int x = 0; x < SIZE; x++){
        if (x == 0){
          if(y % 2) printf("%4u:", y*SCALESHOW);
         else printf("     ");
       }else if(y == 0) {
         if((x-1)%5 == 0) printf("%4d", ((x-46)*SCALESHOW));
         else printf("    ");
       }else printf("  %c ", drawfield[y][x]);
      }
      printf("\n");
    }
    printf("\n");
}

void calcPoint(int lena, double angle, point_t *point)
{
  point->y = lena*sin(angle);
  point->x = lena*cos(angle);
}

void calcPointLookup(int lena, int angle, point_t *point)
{
  point->y = lena*lookupsin[angle]>>7;
  point->x = lena*lookupsin[angle+1]>>7;
}

double distance2wall(point_t *A, point_t *B)
{
  double dydx, b;

  dydx = (A->y - B->y)/(A->x - B->x);
  b = A->y - dydx * A->x;

  return (-b/dydx);
}
