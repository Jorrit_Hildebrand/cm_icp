/* EVA-autorouter.c
 * Creator : Jan-derk Bakker
 * Edited by Jorrit Hildebrand
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>

#define DEBUG 1
#if DEBUG
#define debug(fmt, ...) fprintf(stderr,"[DEBUG]: " fmt, ##__VA_ARGS__)
#else
#define debug(fmt, ...)
#endif
#define DEG2RAD(x) ((x) * M_PI / 180.0)
#define RAD2DEG(x) ((x) * 180.0 / M_PI)
#define LOOKAHEAD_DISTANCE 10.0
#define MAXWAYPOINTS 100


typedef struct {
	double x, y;
	//heading in degrees
	double speed, heading;
} tCar;

static double calcDistance(tCar a, tCar b);
static int loadwaypoints(char *txt, char cutter);
static int GetClosestWaypointIdx(tCar a, int max);
static double Bearing(const tCar orig, const tCar dest);
static void ExtendPath(const tCar orig, double bearing, double dist, tCar *dest);

tCar waypoints[MAXWAYPOINTS];


int main (void)
{
	tCar curPos, targetPos;
	int closestWaypointIdx = 0, totNumWaypoints = 0;
	double routeBearing, carBearing;
	curPos.x = 0;
	curPos.y = 0;
	curPos.heading = 40;
	totNumWaypoints = loadwaypoints("path.txt", ',');

for (size_t i = 0; i < 10; i++) {
	curPos.x++;
	curPos.y++;

	closestWaypointIdx = GetClosestWaypointIdx(curPos, totNumWaypoints);
	if(closestWaypointIdx == totNumWaypoints - 1)
		closestWaypointIdx = totNumWaypoints - 2;
	routeBearing = Bearing(waypoints[closestWaypointIdx], waypoints[closestWaypointIdx + 1]);
	ExtendPath(waypoints[closestWaypointIdx], routeBearing, LOOKAHEAD_DISTANCE, &targetPos);
	carBearing = Bearing(curPos, targetPos);

	fprintf(stdout, ">%.1f<\n", carBearing);
	fflush(stdout);
	fprintf(stderr, "--- pos %f,%f closest to %d (%f,%f) route bearing %.1f target %f,%f car bearing %.1f\n",
		curPos.x, curPos.y, closestWaypointIdx,
		waypoints[closestWaypointIdx].x, waypoints[closestWaypointIdx].y, routeBearing,
		targetPos.x, targetPos.y, carBearing);
		printf("\n\n");
	}
		return 0;
}


static void ExtendPath(const tCar orig, double bearing, double dist, tCar *dest)
{
	double x = cos(DEG2RAD(bearing));
	double y = sin(DEG2RAD(bearing));
	double d = dist / (sqrt(x*x + y*y));
	debug("x: %f, y = %f, d = %f, bearing = %f\n", x,y,d,bearing);

	dest->x = orig.x+x*d;
	dest->y = orig.y+y*d;
} /* ExtendPath */


static double Bearing(const tCar orig, const tCar dest)
{
	double bearing = dest.heading - orig.heading;
	return bearing;
	//return (bearing >= 0.0) ? bearing : bearing + 360.0;
} /* Bearing */


static double calcDistance(tCar a, tCar b)
{
  //return the distance squared between both points
  return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
} /* calcDistance */


static int GetClosestWaypointIdx(tCar a, int max)
{
  double distance = 0, lowest = 1000;
  int position = 0;

  for (int i = 0; i < max; i++) {
    distance  = calcDistance(a, waypoints[i]);
    if (distance < lowest) {
      lowest = distance;
      position = i;
    }
  }
  return position;
}	/* GetClosestWaypointIdx */


static int loadwaypoints(char *txt, char cutter)
{
  char c;

  FILE *fp;
  char temp[24] = {0};
  int tempi = 0, placenumber = 0, xory = 0;

  fp = fopen(txt, "r");

  if (fp == NULL){
      printf("Could not open file");
  }
  else{
    while (!feof(fp)){
      for (int i = 0; i < 24; i++) temp[i] = 0;
      c = fgetc(fp);
      if(c == '#'){
        while (c != '\n' && c != EOF) {
          c = fgetc(fp);
      }
    }
      else{
        while(c != cutter && c != '\n' && c != EOF){
          temp[tempi] = c;
          tempi++;
          c = fgetc(fp);
        }
        xory++;
        tempi = 0;
        if (xory % 2){
          waypoints[placenumber].x = atof(temp);
        }else{
          waypoints[placenumber].y = atof(temp);
					if (placenumber) {
						waypoints[placenumber].heading = RAD2DEG(atan((waypoints[placenumber].x-waypoints[placenumber-1].x)/(waypoints[placenumber].y-waypoints[placenumber-1].y)));
						debug( "atan(%f-%f)/(%f-%f)",waypoints[placenumber].x, waypoints[placenumber-1].x, waypoints[placenumber].y, waypoints[placenumber-1].y);
						debug("place = %d angle %f\n", placenumber,  waypoints[placenumber].heading);
					}
          placenumber++;
        }
      }
    }
    fclose(fp);
  }
	return placenumber;
}
