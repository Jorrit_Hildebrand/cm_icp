/* EVA-autorouter.c. */

/* $GPRMC,162345.000,A,5220.5695812,N,00455.2586604,E,000.14,129.29,170518,,,R*7A */
/* $GNRMC,134446.40,A,5219.06331,N,00453.30732,E,1.669,269.33,220419,,,R,V*1B */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <string.h>


#define RMC_HEADER "$GNRMC,"
#define RMC_VALID 	'A'
#define KNOTS2MTRPERSEC	0.5144444444
#define LINE_LEN 1024
#define RMCARRAY_GROWRATE 16384

#define EARTH_RADIUS 6371e3
#define DEG2RAD(x) ((x) * M_PI / 180.0)
#define RAD2DEG(x) ((x) * 180.0 / M_PI)

#define LOOKAHEAD_DISTANCE 10.0

typedef struct {
	double lat, lon;
	double speed, heading;
} tRMC;


static int GetClosestWaypointIdx(const tRMC *curPosRMC, const tRMC *path, int numWaypoints);
static double Distance(const tRMC *orig, const tRMC *dest);
static double Bearing(const tRMC *orig, const tRMC *dest);
static void ExtendPath(const tRMC *orig, double bearing, double dist, tRMC *dest);
static tRMC *ReadPath_RMC(const char *fileName, int *numRead);
static int Line2RMC(char *line, tRMC *rmc);


int main (int argc, char **argv) {

	tRMC *path = NULL, curPosRMC, targetPosRMC;
	int numWaypoints = 0, valid, closestWaypointIdx;
	double totPathLength, routeBearing, carBearing;
	char buf[LINE_LEN];

	if(argc != 2) {
		fprintf(stderr, "*** Map path file required: %s <map_path.nmea>\n", argv[0]);
		exit(1);
	}

	path = ReadPath_RMC(argv[1], &numWaypoints);
	if(path == NULL) {
		exit(2);
	}
	if(numWaypoints < 2) {
		fprintf(stderr, "*** At least 2 valid RMC waypoints required (got %d)\n", numWaypoints);
		exit(3);
	}

	totPathLength = 0;
	for(int i = 0; i < numWaypoints - 1; i++) {
		totPathLength += Distance(path + i, path + i + 1);
	}
	fprintf(stderr, "=== Map path file %s read, %d waypoints, total length %f\n", 
		argv[1], numWaypoints, totPathLength);

	while(fgets(buf, LINE_LEN, stdin) != NULL) {
		int isLastWaypoint = 0;
		valid = Line2RMC(buf, &curPosRMC);
		if(valid) {
			closestWaypointIdx = GetClosestWaypointIdx(&curPosRMC, path, numWaypoints);
			/* For now we can't deal with being closest to the final waypoint */
			if(closestWaypointIdx == numWaypoints - 1) {
				isLastWaypoint = 1;
				closestWaypointIdx = numWaypoints - 2;
			}
			routeBearing = Bearing(path + closestWaypointIdx, path + closestWaypointIdx + 1);
			ExtendPath(path + closestWaypointIdx, routeBearing, LOOKAHEAD_DISTANCE, &targetPosRMC);
			carBearing = Bearing(&curPosRMC, &targetPosRMC);
			fprintf(stdout, ">%.1f<[%d]\n", carBearing, isLastWaypoint);
			fflush(stdout);
			fprintf(stderr, "--- pos %f,%f closest to %d %s(%f,%f) route bearing %.1f target %f,%f car bearing %.1f\n",
				curPosRMC.lat, curPosRMC.lon, closestWaypointIdx, isLastWaypoint ? "LAST WYPT " : "",
				path[closestWaypointIdx].lat, path[closestWaypointIdx].lon, routeBearing,
				targetPosRMC.lat, targetPosRMC.lon, carBearing);
		}
	}
	
	return 0;
}


static int GetClosestWaypointIdx(const tRMC *curPosRMC, const tRMC *path, int numWaypoints) {

	int res = 0, i;
	double curDistance, bestDistance = Distance(curPosRMC, path);

	for(i = 1; i < numWaypoints; i++) {
		curDistance = Distance(curPosRMC, path + i);
		if(curDistance < bestDistance) {
			bestDistance = curDistance;
			res = i;
		}
	}

	return res;

} /* GetClosestWaypointIdx */

// Reference for all Haversine stuff: https://www.movable-type.co.uk/scripts/latlong.html

static double Distance(const tRMC *orig, const tRMC *dest) {

	double phi1 = DEG2RAD(orig->lat);
	double phi2 = DEG2RAD(dest->lat);
	double deltaphi = DEG2RAD(dest->lat - orig->lat);
	double deltalambda = DEG2RAD(dest->lon - orig->lon);

	double a = sin(deltaphi / 2.0) * sin(deltaphi / 2.0)
				+ cos(phi1) * cos(phi2)
				* sin(deltalambda / 2) * sin(deltalambda / 2);
	double c = 2.0 * atan2(sqrt(a), sqrt(1.0 - a));

	return EARTH_RADIUS * c;

} /* Distance */


static double Bearing(const tRMC *orig, const tRMC *dest) {

	double phi1 = DEG2RAD(orig->lat);
	double phi2 = DEG2RAD(dest->lat);
	double deltalambda = DEG2RAD(dest->lon - orig->lon);

	double y = sin(deltalambda) * cos(phi2);
	double x = cos(phi1) * sin(phi2) - sin(phi1) * cos(phi2) * cos(deltalambda);

	double bearing = RAD2DEG(atan2(y, x));

	return (bearing >= 0.0) ? bearing : bearing + 360.0; 

} /* Bearing */


static void ExtendPath(const tRMC *orig, double bearing, double dist, tRMC *dest) {

	double phi1 = DEG2RAD(orig->lat);
	double lambda1 = DEG2RAD(orig->lon);

	double phi2 = asin(sin(phi1) * cos(dist / EARTH_RADIUS)
					+ cos(phi1) * sin(dist / EARTH_RADIUS) * cos(DEG2RAD(bearing)));
	double lambda2 = lambda1 + atan2(sin(DEG2RAD(bearing)) * sin(dist / EARTH_RADIUS) * cos(phi1),
									cos(dist / EARTH_RADIUS) - sin(phi1) * sin(phi2));

	/* Normalize the longitude */
	lambda2 += 3.0 * M_PI;
	while(lambda2 > 2.0 * M_PI)
		lambda2 -= 2.0 * M_PI;
	lambda2 -= M_PI;

	assert(dest != NULL);

	dest->lat = RAD2DEG(phi2);
	dest->lon = RAD2DEG(lambda2);

} /* ExtendPath */


static tRMC *ReadPath_RMC(const char *fileName, int *numRead) {
	tRMC *res = NULL, curRMC;
	int resCapacity = 0, lineNo = 0,validRMC;
	FILE *inFile = NULL;
	char buf[LINE_LEN];

	*numRead = 0;

	inFile = fopen(fileName, "r");
	if(inFile == NULL) {
		perror("*** Opening RMC map path file");
		return NULL;
	}
	while(fgets(buf, LINE_LEN, inFile) != NULL) {
		lineNo++;
		validRMC = Line2RMC(buf, &curRMC);
		if(validRMC) {
			if(resCapacity <= *numRead) {
				resCapacity += RMCARRAY_GROWRATE;
				res = (tRMC *) realloc(res, resCapacity * sizeof(tRMC));
				assert(res != NULL);
			}
			res[*numRead] = curRMC;
			(*numRead)++;			
		}
	}
	
	fprintf(stderr, "=== %d RMC waypoints read in %d lines\n", *numRead, lineNo);
	fclose(inFile);
	return res;

} /* ReadPath_RMC */


static int Line2RMC(char *line, tRMC *rmc) {

	tRMC tempRMC;
	int valid = 1;
	char *here;
	char latlonbuf[4];

	if(strstr(line, RMC_HEADER) != line)
		valid = 0;

	/* Maybe do checksumming here */

	if(valid) {
		here = line + strlen(RMC_HEADER);
		while(*here != ',' && *here != '\0')
			here++;
		if(*here != ',')
			valid = 0;
	}
	if(valid) {
		here++;
		if(*here != RMC_VALID)
			valid = 0;
	}
	if(valid) {
		here++;
		if(*here != ',')
			valid = 0;
	}
	if(valid) {
		here++;
		if(here[4] == '.'){
			latlonbuf[0] = here[0];
			latlonbuf[1] = here[1];
			latlonbuf[2] = '\0';
			tempRMC.lat = strtod(latlonbuf, NULL) + strtod(here + 2, &here) / 60.0;
		}
		else{
			tempRMC.lat =strtod(here,&here);
		}
		if(*here != ',')
			valid = 0;
	}
	if(valid) {
		here++;
		if(toupper(*here) == 'S')
			tempRMC.lat *= -1;
		else if(toupper(*here) != 'N')
			valid = 0;
	}
	if(valid) {
		here++;
		if(*here != ',')
			valid = 0;
	}
	if(valid) {
		here++;
		if(here[5] == '.'){
			latlonbuf[0] = here[0];
			latlonbuf[1] = here[1];
			latlonbuf[2] = here[2];
			latlonbuf[3] = '\0';
			tempRMC.lon = strtod(latlonbuf, NULL) + strtod(here + 3, &here) / 60.0;
		}
		else{
			tempRMC.lon = strtod(here, &here);
		}
		if(*here != ',')
			valid = 0;
	}
	if(valid) {
		here++;
		if(toupper(*here) == 'W')
			tempRMC.lon *= -1;
		else if(toupper(*here) != 'E')
			valid = 0;
	}/*
	if(valid) {
		here++;
		if(*here != ',')
			valid = 0;
	}
	if(valid) {
		here++;
		tempRMC.speed = strtod(here, &here) * KNOTS2MTRPERSEC;
		if(*here != ',')
			valid = 0;
	}
	if(valid) {
		here++;
		tempRMC.heading = strtod(here, &here);
		if(*here != ',')
			valid = 0;
	}*/

	if(valid)
		*rmc = tempRMC;

	return valid;

} /* Line2RMC */
